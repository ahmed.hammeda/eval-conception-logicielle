# Partie Webservice

Cette partie concerne la mise en place d'une fastAPI. 
Pour la lancer, rendez vous à la branche api avec `cd api/` et exécutez ce code dans le terminal:

# installer les dépendances
```
pip3 install -r requirements.txt
```

# lancer la FastAPI
```
python3 main.py
```

