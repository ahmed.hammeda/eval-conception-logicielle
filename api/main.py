from fastapi import FastAPI
import uvicorn
import requests
from pydantic import BaseModel

app = FastAPI()

class Deck(BaseModel):
    deck_id: str
  
@app.get("/")
#fonction à la racine du webservice
def read_root():
    return {"Hello": "World"}

@app.get("/creer-un-deck/")
#fonction qui recupere un deck de l'api et renvoie son id
def create_deck():
    r = requests.get("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    x = r.json()
    deck_id = x["deck_id"]
    return deck_id

@app.post("/cartes/{nombre_cartes}")
#fonction qui tire un nombre de cartes choisis à partir d'un deck
def draw_cards(nombre_cartes: int, Deck: dict):
    str_n_cards = "{}".format(nombre_cartes)
    r=requests.get("https://deckofcardsapi.com/api/deck/"+Deck["deck_id"]+"/draw/?count="+str_n_cards)
    x = r.json()
    return(x)

#lancer le webservice dans le localhost  
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

