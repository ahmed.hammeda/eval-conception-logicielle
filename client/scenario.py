from main import fonctions

#lancer le scénario avec 10 cartes tirées
if __name__ == "__main__":
    scenario = True
    while scenario :
        #creation du deck
        deck = fonctions.create_deck()
        print("The id of the new deck is : {} \n".format(deck["deck_id"]))
        #tirer 10 cartes à partir du deck créé
        print("We have drawn 10 cards for you!")
        ten_cards = fonctions.draw_cards(deck, 10)
        ListOfCards = ten_cards["cards"]
        #calcul des couleurs dans les cartes tirées
        colors = fonctions.count_colors(ListOfCards)
        print("The colors of the drawn cards are : {}\n".format(colors))
        print("This is all with the scenario!\n")
        scenario = False