from main import fonctions
import json

#On teste la fonction de calcul de couleurs pour un exemple de liste de cartes
def test_count():
    #Exemple de 3 cartes mis dans un fichier json pour tester la fonction
    with open('client/test.json') as json_data:  
        data = json.load(json_data) 
    #Le résultat attendu si la fonction est correcte
    assert fonctions.count_colors(data) == {"H": 1, "S": 1, "D": 0, "C": 1} 



