import requests

class fonctions:
    #fct du creation du deck qui renvoie son id
    def create_deck():
        r=requests.get("http://127.0.0.1:8000/creer-un-deck/")
        x=r.json()
        return {"deck_id": x}

    #fct qui tire un nombre choisi de cartes depuis le deck déjà créé
    def draw_cards(Deck, nombre_cartes):
        str_nombre_cartes = "{}".format(nombre_cartes)
        r=requests.post("http://127.0.0.1:8000/cartes/"+str_nombre_cartes, json=Deck)
        x=r.json()
        return {"deck_id" : x["deck_id"], "remaining" : x["remaining"], "cards":x["cards"]}

    #fonction qui calcule le nombre de chaque couleurs dans les cartes tirées
    def count_colors(cards_list):
        length = len(cards_list)
        hearts=0
        spades=0
        diamonds=0
        clubs=0
        for i in range(0,length):
            if cards_list[i]["suit"] == "HEARTS":
                hearts +=1
            elif cards_list[i]["suit"] == "SPADES":
                spades +=1
            elif cards_list[i]["suit"] == "DIAMONDS":
                diamonds +=1
            else :
                clubs += 1     
        colors = {"H":hearts, "S" : spades, "C" : clubs, "D" : diamonds}
        return colors

#lancer un scénario où l'utilisateur choisis le nombre de cartes tirées
if __name__ == "__main__":
    """Game Starting!"""
    print("Welcome!")
    # Initialisation du deck
    deck = fonctions.create_deck()
    print("We have initialised a deck for you with the id : {} \n".format(deck["deck_id"]))
    # Tirer des cartes
    print("Enter the number of cards you want to draw!")
    # l'utilisateur insere le nombre de cartes qu'il veut tirer
    nombre_cartes = input("> ")
    drawn_cards = fonctions.draw_cards(deck,nombre_cartes)
    #calcul des couleurs des cartes tirées
    ListOfCards = drawn_cards["cards"]   
    colors = fonctions.count_colors(ListOfCards)
    print("This is the number of each color your drawn cards have: {}\n".format(colors))

