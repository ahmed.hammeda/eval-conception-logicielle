# Partie Client

Cette partie concerne les requêtes du client sur l'API. 

Pour en profiter, lancez la partie webservice. Puis, dans un autre terminal, rendez vous à la branche client avec `cd client/` et exécuter le code ci-dessous.

## Installer les dépendances 

```
pip3 install -r requirements.txt
```

## Lancer l'application avec un scénario où vous choisissez le nombre de cartes à tirer

```
python3 main.py
```
## Lancer l'application avec un scénario prédéfini avec 10 cartes tirées

```
python3 scenario.py
```

## Tests unitaires

Les tests unitaires s'exécutent automatiquement grâce à la pipeline.
