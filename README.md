# Jeu de Cartes

Bienvenue dans notre application!


## Objectif de l'application

L'application a deux objectifs. D'une part, la réalisation un webservice pouvant récupérer, depuis une API, un deck de 52 cartes et tirage des cartes à partir de ce dernier. D'autre part, mise en place d'un client de ce webservice capable de requéter cette API.
Dans ce contexte, on a élaboré deux scénarios pour le client:
- Scenario automatique: Création d'un deck et tirage de 10 cartes avec comptage de couleurs.
- Scenario manuel: Le même principe avec l'option: Le client choisit le nombre de cartes à tirer.

## Organisation et fonctionnement du code

Cette application s'organise en deux dossiers :
- la partie webservice
- la partie client

#### La partie webservice

Cette partie consiste en la création d'une FastAPI.
Ce webservice est lui même un client web pour l'api https://deckofcardsapi.com/ et il comporte 3 endpoints : 
- Le premier exposé en GET est un simple affichage à la racine : http://localhost:8000/
- Le second exposé en GET récupère un deck de l'api et renvoie son id : http://localhost:8000/creer-un-deck/
- Le troisième exposé en POST tire des cartes du deck : http://localhost:8000//cartes/{nombre_cartes}

#### La partie client

La partie client permet de requêter l'API DeckOfCards via le webservice créé. Elle permet de réaliser:
- La mise en place d'un jeu de 52 cartes via l'API DeckOfCards et retourne l'identifiant du jeu de carte (ou deck).
- Le tirage de 10 cartes(ou un nombre choisi par le client) dans le deck et retourne la liste des cartes tirées dans un dictionnaire au format json.
- Le calcul du nombre de cartes par couleur parmi les cartes tirées. 

## Quick start

Pour lancer rapidement l'application veuillez créer deux terminaux :

- Sur le premier, rendez vous à la branche api avec `cd api/` et exécutez ce code:

#### installer les dépendances
```
pip3 install -r requirements.txt
```

#### lancer la FastAPI
```
python3 main.py
```

- Une fois la FastAPI mise en route, sur le deuxième terminal, rendez vous à la branche client avec `cd client/` et exécuter le code ci-dessous.

#### Installer les dépendances 

```
pip3 install -r requirements.txt
```

#### Lancer l'application avec un scénario où vous choisissez le nombre de cartes à tirer

```
python3 main.py
```
#### Lancer l'application avec un scénario prédéfini avec 10 cartes tirées

```
python3 scenario.py
```

